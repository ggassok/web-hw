from django.shortcuts import render
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
import random

import json
from django.contrib.auth import authenticate, login as auth_login, logout as auth_logout
from django.contrib.auth.decorators import login_required
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.http import Http404
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views import View

from app.models import *
from .forms import *


# Create your views here.
# members
popular_members = [
    {'name': 'Boba'},
    {'name': 'Nastenka'},
    {'name': 'Ladislav_Kovac'},
    {'name': 'F4lkram'},
    {'name': 'kdv'},
]

# tags
tags = [
    {'name': 'C',
     'priority': 'high'},
    {'name': 'Python',
     'priority': 'medium'},
    {'name': 'Pascal',
     'priority': 'low'},
    {'name': 'Ruby',
     'priority': 'very high'},
    {'name': 'C',
     'priority': 'high'},
    {'name': 'Java',
     'priority': 'medium'},
    {'name': 'PHP',
     'priority': 'low'},
    {'name': 'Go',
     'priority': 'very high'},
    {'name': 'Django',
     'priority': 'high'},
    {'name': 'Bootstrap',
     'priority': 'medium'},
    {'name': 'C',
     'priority': 'low'},
    {'name': 'Qt',
     'priority': 'very high'}
]

# set True for logged in  user view and False for not
def fillErrors(formErrors, errors):
    for i in formErrors:
        formattedFieldName = i.replace('_', ' ')
        errors.append(f' { formattedFieldName } field error: {formErrors[i][0]}')

def paginate(objects_list, request, per_page=10):
    paginator = Paginator(objects_list, per_page)
    page_number = request.GET.get('page')
    try:
        page_obj = paginator.get_page(page_number)
    except PageNotAnInteger:
        page_obj = paginator.get_page(1)
    except EmptyPage:
        page_obj = paginator.get_page(paginator.num_pages)

    return page_obj

@login_required(login_url='/login/')
def profile(request, username):
    user = User.objects.by_username(username)
    if user is not None:
        return render(request, 'profile.html', {'profile': user})
    else:
        raise Http404


def index(request):
    return render(request, 'base.html',
                  {'page': paginate(Question.objects.newest(), request), 'profile': profile, 'popular_members': popular_members,
                   'popular_tags': tags})


def hot(request):
    return render(request, 'hot.html',
                  {'page': paginate(Question.objects.hottest(), request), 'profile': profile, 'popular_members': popular_members,
                   'popular_tags': tags})


def signup(request):
    errors = []
    form = UserSignUpForm
    if request.method == 'POST':
        form = form(request.POST)
        if request.POST['password'] != request.POST['password_confirmation']:
            errors.append('Passwords don\'t match')
        elif form.is_valid():
            user = User.objects.create_user(username=request.POST['username'],
                                       email=request.POST['email'],
                                       first_name=request.POST['first_name'],
                                       last_name=request.POST['last_name'])
            user.set_password(request.POST['password_confirmation'])
            user.save()
            auth_login(request, user)
            return redirect('/')
        else:
            fillErrors(form.errors, errors)
    else:
        auth_logout(request)

    return render(request, 'signup.html', {'form': form, 'messages': errors})

def AnswStatusChange(request):
    c_user = request.user
    a_id = request.POST.get('val')
    status = request.POST.get('stat')
    cur_answ = Answer.objects.get(id=a_id)
    if (cur_answ.status == True):
        cur_answ.status = False
        cur_answ.save()
        return HttpResponse(json.dumps({'status': 'ok', 'score': False}), content_type='application/json')
    else:
        cur_answ.status = True
        cur_answ.save()
        return HttpResponse(json.dumps({'status': 'ok', 'score': True}), content_type='application/json')

def login(request):
    errors = []
    form = UserSignInForm
    if request.method == 'POST':
        username = request.POST['username']
        password = request.POST['password']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            auth_login(request, user)
            return redirect(request.GET.get('next') if request.GET.get('next') != '' else '/')
        else:
            errors.append('Invalid username or password')

        auth_logout(request)
    return render(request, 'login.html', {'popular_members': popular_members,
                                        'popular_tags': tags, 'form': form, 'messages': errors})

def logout(request):
    if not request.user.is_authenticated:
        raise Http404
    auth_logout(request)
    return redirect(request.GET['from'])


@login_required(login_url='/login/')
def ask(request):
    errors = []
    form = NewQuestionForm
    if request.method == 'POST':
        form = form(request.POST)

        if form.is_valid():
            new_question = Question.objects.create(author=request.user,
                                               date=timezone.now(),
                                               is_active=True,
                                               title=request.POST['title'],
                                               text=request.POST['text'])
            new_question.save()
            for tagTitle in request.POST['tags'].split():
                tag = Tag.objects.get_or_create(title=tagTitle)[0]
                new_question.tags.add(tag)
                new_question.save()
            return question(request, new_question.id, 1)
        else:
            fillErrors(form.errors, errors)

    return render(request, 'ask.html', {'form': form, 'messages': errors})


@login_required(login_url='/login/')
def settings(request):
    errors = []
    form = UserSettingsForm
    if request.method == 'POST':
        form = form(data=request.POST,
                    files=request.FILES)
        if form.is_valid():
            for changedField in form.changed_data:
                if changedField == 'avatar':
                    setattr(request.user, changedField, request.FILES[changedField])
                else:
                    setattr(request.user, changedField, request.POST[changedField])
            request.user.save()
            return redirect('/settings/')
        else:
            fillErrors(form.errors, errors)
    else:
        for i in form.base_fields:
            form.base_fields[i].widget.attrs['placeholder'] = getattr(request.user, i)
    return render(request, 'settings.html', {'form': form, 'messages': errors})

def question(request, pk, new=0):
    curr_question = Question.objects.by_id(int(pk)).first()
    if curr_question is not None:
        answers = curr_question.answers.hottest()
    else:
        raise Http404

    if (curr_question and answers) is not None:
        errors = []
        form = WriteAnswerForm
        if Question.objects.filter(id=pk).exists():
            if request.method == 'POST' and new == 0:
                form = form(request.POST)
                if form.is_valid():
                    answeredQuestion = Question.objects.by_id(pk)[0]
                    newAnswer = Answer.objects.create(author=request.user,
                                                      date=timezone.now(),
                                                      text=request.POST['text'],
                                                      question_id=answeredQuestion.id)
                    newAnswer.save()
                    pages = Paginator(answers, 10)
                    return redirect(f'/question/{pk}/?page={pages.num_pages}#{newAnswer.id}')
                else:
                    fillErrors(form.errors, errors)

            return render(request, 'question.html',
                  {'page': paginate(answers, request), 'main': curr_question, 'profile': profile,
                  'popular_members': popular_members,
                  'popular_tags': tags, 'form': form})
    else:
        raise Http404


def tag(request, tag_name):
    by_tag_sorted_questions = []
    # sort questions by tag
    for question in Question.objects.all():
        for tag in question.tags.all():
            if (tag.title == tag_name):
                by_tag_sorted_questions.append(question)
    return render(request, 'tag_index.html',
                  {'page': paginate(by_tag_sorted_questions, request), 'profile': profile, 'tag_name': tag_name, 'popular_members': popular_members,
                   'popular_tags': tags})

@login_required(login_url='/login/')
def profile(request, username):
    user = User.objects.by_username(username)
    if user is not None:
        return render(request, 'profile.html', {'profile': user})
    else:
        raise Http404

class VotesView(View):
    model = None
    vote_type = None

    def post(self, request, pk):
        obj = self.model.objects.get(pk=pk)
        try:
            like_dislike = Like.objects.get(content_type=ContentType.objects.get_for_model(obj),
                                            object_id=obj.id,
                                            user=request.user)

            if like_dislike.vote is not self.vote_type:
                like_dislike.vote = self.vote_type
                obj.rate += 2*self.vote_type
                obj.author.rank += 2*self.vote_type
                like_dislike.save(update_fields=['vote'])
                result = True
            else:
                obj.rate -= self.vote_type
                like_dislike.delete()
                result = False
        except Like.DoesNotExist:
            obj.votes.create(user=request.user, vote=self.vote_type)
            obj.rate += self.vote_type
            obj.author.rank += self.vote_type
            result = True

        obj.save()
        obj.author.save()
        return HttpResponse(
            json.dumps({
                "result": result,
                "like_count": obj.votes.likes().count(),
                "dislike_count": obj.votes.dislikes().count(),
                "sum_rating": obj.votes.sum_rating()
            }),
            content_type="application/json"
        )
