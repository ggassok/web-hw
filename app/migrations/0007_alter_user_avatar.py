# Generated by Django 3.2 on 2021-05-21 21:18

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0006_alter_user_avatar'),
    ]

    operations = [
        migrations.AlterField(
            model_name='user',
            name='avatar',
            field=models.ImageField(default='static/img/1.jpg', upload_to='static/upload/'),
        ),
    ]
