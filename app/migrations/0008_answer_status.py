# Generated by Django 3.2 on 2021-05-22 14:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0007_alter_user_avatar'),
    ]

    operations = [
        migrations.AddField(
            model_name='answer',
            name='status',
            field=models.BooleanField(default=False, verbose_name='Статус'),
        ),
    ]
