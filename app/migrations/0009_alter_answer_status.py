# Generated by Django 3.2 on 2021-05-22 15:53

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0008_answer_status'),
    ]

    operations = [
        migrations.AlterField(
            model_name='answer',
            name='status',
            field=models.BooleanField(default=False, verbose_name='Status'),
        ),
    ]
