from django.conf import settings
import os
from uuid import uuid4
from django.contrib.contenttypes.models import ContentType
from django.contrib.auth.models import AbstractUser
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.contrib.contenttypes.fields import GenericRelation
from django.utils import timezone
# Create your models here.

from app.managers import *

class User(AbstractUser):
    avatar = models.ImageField(default="static/img/1.jpg", upload_to='static/upload/')
    rank = models.IntegerField(default=0, verbose_name='User rating')

    objects = UserManager()


    class Meta:
        verbose_name = 'Пользователь'
        verbose_name_plural = 'Пользователи'

    def __str__(self):
        return self.username


class Tag(models.Model):
    title = models.CharField(max_length=15, default='404', verbose_name='Tag')

    objects = TagManager()

    class Meta:
        verbose_name = 'Тэг'
        verbose_name_plural = 'Тэги'
        unique_together = ('title',)

    def __str__(self):
        return self.title



class Like(models.Model):

    LIKE = 1
    DISLIKE = -1

    VOTE_TYPES = ((LIKE, 'Like'), (DISLIKE, 'Dislike'))

    user = models.ForeignKey(User, null=True, verbose_name='Like author', on_delete=models.CASCADE)
    vote = models.SmallIntegerField(verbose_name='is like', default=VOTE_TYPES[0], choices=VOTE_TYPES)

    content_type = models.ForeignKey(ContentType, default=None, on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(default=-1)
    content_object = GenericForeignKey()

    objects = LikeManager()

    class Meta:
        verbose_name = 'Лайк'
        verbose_name_plural = 'Лайки'

    def __str__(self):
        return "Like from " + self.user.username
#
#
class Question(models.Model):
    author = models.ForeignKey(User, null=False, verbose_name='Question author', on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now, verbose_name='Question date')

    is_active = models.BooleanField(default=True, verbose_name='Question aviability')

    title = models.CharField(max_length=70, verbose_name='Header')
    text = models.TextField(verbose_name='Question full text')

    tags = models.ManyToManyField(Tag, related_name='questions', blank=True, verbose_name='Tags')

    votes = GenericRelation(Like, related_query_name='questions')
    rate = models.IntegerField(default=0, null=False, verbose_name='Rate')

    type = 'question'

    objects = QuestionManager()

    class Meta:
        verbose_name = 'Вопрос'
        verbose_name_plural = 'Вопросы'

    def __str__(self):
        return self.title


class Answer(models.Model):
    author = models.ForeignKey(User, null=False, verbose_name='Answer author', on_delete=models.CASCADE)
    date = models.DateTimeField(default=timezone.now, verbose_name='Answer date')

    question = models.ForeignKey(Question, related_name='answers', verbose_name='Answered question',
                                 on_delete=models.CASCADE)
    text = models.TextField(verbose_name='Answer full text')
    status = models.BooleanField(verbose_name=u"Status", default=False)
    votes = GenericRelation(Like, related_query_name='answers')
    rate = models.IntegerField(default=0, null=False, verbose_name='Rate')

    type = 'answer'

    objects = AnswerManager()


    class Meta:
        verbose_name = 'Ответ'
        verbose_name_plural = 'Ответы'

    def __str__(self):
        return self.text
